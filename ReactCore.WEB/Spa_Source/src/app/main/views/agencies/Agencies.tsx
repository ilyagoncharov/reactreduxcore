import * as React                                        from 'react';
import { connect }                                       from 'react-redux';
import { IAppState }                                     from '../../../../store';
import { IAgenciesState }                                from '../../../../store/agencies/types';
import { GetPropsStoreRoute }                            from '../../../types';
import { actionAgenciesAdd, actionCreatorAgenciesClear } from '../../../../store/agencies/actions';
import { UserDetails }                                   from '../../../../models/dto.models';

type Props = GetPropsStoreRoute<IAgenciesState>;

class Agencies extends React.Component<Props>
{
  public constructor(props: Props)
  {
    super(props);
  }

  componentDidMount()
  {
    this.props.dispatch(actionAgenciesAdd());
  }

  componentWillUnmount()
  {
    this.props.dispatch(actionCreatorAgenciesClear())
  }

  render()
  {
    return (
      <div className='flex-container'>
        {
          this.props.agencies.map((user: UserDetails) =>
          {
            return <div key={user.id}>{user.fullName}</div>;
          })
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => state.agencies;

export default connect(mapStateToProps)(Agencies);
