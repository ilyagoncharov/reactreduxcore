import * as React                       from 'react';
import { GetPropsStoreRoute }           from '../../../types';
import { IVolunteersState }             from '../../../../store/volunteers/types';
import { UserDetails }                  from '../../../../models/dto.models';
import { IAppState }                    from '../../../../store';
import { connect }                      from 'react-redux';
import { actionCreatorVolunteersClear } from '../../../../store/volunteers/actions';
import { sagaActionVolunteersAdd }      from '../../../../store/volunteers/sagas';

type Props = GetPropsStoreRoute<IVolunteersState>;

class Volunteers extends React.Component<Props>
{
  componentDidMount()
  {
    this.props.dispatch(sagaActionVolunteersAdd());
  }

  componentWillUnmount()
  {
    this.props.dispatch(actionCreatorVolunteersClear());
  }

  render()
  {
    return (
      <div className='flex-container'>
        {
          this.props.volunteers.map((user: UserDetails) =>
          {
            return <div key={user.id}>{user.fullName}</div>;
          })
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => state.volunteers;

export default connect(mapStateToProps)(Volunteers);
