import * as React                                        from 'react';
import { GetPropsStoreRoute }                            from '../../../types';
import { IManagersState }                                from '../../../../store/managers/types';
import { actionManagersAdd, actionCreatorManagersClear } from '../../../../store/managers/actions';
import { UserDetails }                                   from '../../../../models/dto.models';
import { IAppState }                                     from '../../../../store';
import { connect, Dispatch }                             from 'react-redux';

interface IDispatchToProps
{
  actionManagersAdd: () => void,
  actionManagersClear: () => void
}

type Props = GetPropsStoreRoute<IManagersState, IDispatchToProps>;

class Managers extends React.Component<Props>
{
  componentDidMount()
  {
    this.props.actionManagersAdd();
  }

  componentWillUnmount()
  {
    this.props.actionManagersClear();
  }

  render()
  {
    return (
      <div className='flex-container'>
        {
          this.props.managers.map((user: UserDetails) =>
          {
            return <div key={user.id}>{user.fullName}</div>
          })
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => state.managers;

const mapDispatchToProps = (dispatch: Dispatch<IAppState>): IDispatchToProps => ({
  actionManagersAdd: () => dispatch(actionManagersAdd()),
  actionManagersClear: () => dispatch(actionCreatorManagersClear())
});

export default connect(mapStateToProps, mapDispatchToProps)(Managers);
