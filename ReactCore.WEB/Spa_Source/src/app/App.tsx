import * as React                from 'react';
import { browserHistory, State } from '../store/configureStore';
import { Provider }              from 'react-redux';
import { ConnectedRouter }       from 'react-router-redux';
import { AppRoutes }             from '../routing/Routes';

export const App = () => (
  <Provider store={State.store}>
    <ConnectedRouter history={browserHistory}>
      <AppRoutes/>
    </ConnectedRouter>
  </Provider>
);
