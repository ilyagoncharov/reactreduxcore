function getPage(page: IBasePage): IPage
{
  const path = page.params
    ? page.route + page.params
    : page.route;

  return {
    route: page.route,
    params: page.params,
    title: page.title,
    path: path
  };
}

interface IBasePage
{
  route: string,
  params?: string
  title: string;
}

export interface IPage extends IBasePage
{
  path: string;
}

export const PageDefault: IBasePage = {
  route: '/',
  title: ''
};

export const PageAuth: IBasePage = {
  route: '/auth',
  title: ''
};

export const PageLogin: IBasePage = {
  route: PageAuth.route + '/login',
  title: 'Login'
};

export const PageRegister: IBasePage = {
  route: PageAuth.route + '/register',
  title: 'Register'
};

export const PageMain: IBasePage = {
  route: '/main',
  title: ''
};

export const PageRequests: IBasePage = {
  route: PageMain.route + '/requests',
  title: 'Pending Children'
};

export const PageAgencies: IBasePage = {
  route: PageMain.route + '/agencies',
  title: 'Agencies'
};

export const PageAgency: IBasePage = {
  route: PageMain.route + '/agency',
  params: '/:id',
  title: 'Agency'
};

export const PageDeliveries: IBasePage = {
  route: PageMain.route + '/deliveries',
  title: 'Deliveries'
};

export const PageVolunteers: IBasePage = {
  route: PageMain.route + '/volunteers',
  title: 'Volunteers'
};

export const PageManagers: IBasePage = {
  route: PageMain.route + '/managers',
  title: 'Managers'
};

export class Page
{
  public static readonly default: IPage = getPage(PageDefault);

  public static readonly auth: IPage = getPage(PageAuth);
  public static readonly login: IPage = getPage(PageLogin);
  public static readonly register: IPage = getPage(PageRegister);

  public static readonly main: IPage = getPage(PageMain);
  public static readonly requests: IPage = getPage(PageRequests);
  public static readonly agencies: IPage = getPage(PageAgencies);
  public static readonly agency: IPage = getPage(PageAgency);
  public static readonly deliveries: IPage = getPage(PageDeliveries);
  public static readonly volunteers: IPage = getPage(PageVolunteers);
  public static readonly managers: IPage = getPage(PageManagers);

  public static findPage(path: string): IPage
  {
    const pages: IPage[] = [];

    Object.keys(Page).forEach((key) =>
    {
      const page = <IPage>Page[key];

      if (typeof page === 'object')
      {
        if (path.indexOf(page.route) > -1)
        {
          pages.push(page);
        }
      }
    });

    const result = pages.reduce((x: IPage, y: IPage) =>
    {
      return x.route.length > y.route.length ? x : y;
    });

    return result;
  }
}
