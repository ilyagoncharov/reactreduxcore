import { ActionCreator, Dispatch }        from 'redux';
import { IActionPageSet, PageActionType } from './types';
import { IAppState }                      from '../index';
import { Page }                           from '../../routing/Page';

export const actionCreatorPageSet: ActionCreator<IActionPageSet> = (title: string) => ({
  type: PageActionType.PAGE_SET,
  payload: title
});

export const actionPageSet = (location: Location) =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    const page = Page.findPage(location.pathname);
    document.title = page.title;
    dispatch(actionCreatorPageSet(page.title));
  }
};
