import { IPageState, PageActionType } from './types';
import { IBaseAction }                from '../index';
import { State }                      from '../configureStore';
import { actionPageSet }              from './actions';

const initialState: IPageState = {
  title: ''
};

export const pageReducer = (state: IPageState = initialState, action: IBaseAction<PageActionType>): IPageState =>
{
  switch (action.type)
  {
    case PageActionType.LOCATION_CHANGE:
      setTimeout(() =>
      {
        State.dispatch(actionPageSet(action.payload));
      });
      return state;
    case PageActionType.PAGE_SET:
      return {...state, title: action.payload};
    default:
      return state;
  }
};
