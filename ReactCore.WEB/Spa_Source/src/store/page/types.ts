import { IBaseAction } from '../index';

export interface IPageState
{
  title: string;
}

export enum PageActionType
{
  LOCATION_CHANGE = '@@router/LOCATION_CHANGE',
  PAGE_SET = 'PAGE_SET'
}

export interface IActionPageSet extends IBaseAction<PageActionType>
{
  type: PageActionType.PAGE_SET,
  payload: string
}
