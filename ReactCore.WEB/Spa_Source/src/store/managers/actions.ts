import { AdminManagerService }                                          from '../../services/webapi/admin-manager.service';
import { ActionCreator, Dispatch }                                      from 'redux';
import { IActionManagersAdd, IActionManagersClear, ManagersActionType } from './types';
import { UserDetails }                                                  from '../../models/dto.models';
import { IAppState }                                                    from '../index';

const service = new AdminManagerService();

export const actionCreatorManagersAdd: ActionCreator<IActionManagersAdd> = (users: UserDetails[]) => ({
  type: ManagersActionType.MANAGERS_ADD,
  payload: users
});

export const actionCreatorManagersClear: ActionCreator<IActionManagersClear> = () => ({
  type: ManagersActionType.MANAGERS_CLEAR
});

export const actionManagersAdd = () =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    service.list().subscribe((users: UserDetails[]) =>
    {
      dispatch(actionCreatorManagersAdd(users));
    })
  }
};
