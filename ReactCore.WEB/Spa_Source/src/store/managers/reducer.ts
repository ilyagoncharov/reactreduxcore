import { IManagersState, ManagersActionType } from './types';
import { IBaseAction }                        from '../index';

const initialState: IManagersState = {
  managers: []
};

export const managersReducer = (state: IManagersState = initialState, action: IBaseAction<ManagersActionType>): IManagersState =>
{
  switch (action.type)
  {
    case ManagersActionType.MANAGERS_ADD:
      return {...state, managers: [...state.managers, ...action.payload]};
    case ManagersActionType.MANAGERS_CLEAR:
      return {...state, managers: []};
    default:
      return state;
  }
};
