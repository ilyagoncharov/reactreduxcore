import { UserDetails }        from '../../models/dto.models';
import { IBaseAction }        from '../index';

export interface IManagersState
{
  managers: UserDetails[];
}

export enum ManagersActionType
{
  MANAGERS_ADD = 'MANAGERS_ADD',
  MANAGERS_CLEAR = 'MANAGERS_CLEAR'
}

export interface IActionManagersAdd extends IBaseAction<ManagersActionType>
{
  type: ManagersActionType.MANAGERS_ADD,
  payload: UserDetails[]
}

export interface IActionManagersClear extends IBaseAction<ManagersActionType>
{
  type: ManagersActionType.MANAGERS_CLEAR
}
