import { watchVolunteersAdd } from './volunteers/sagas';
import { SagaMiddleware }     from 'redux-saga';

export const sagasRun = (sagaMiddleware: SagaMiddleware<object>) =>
{
  sagaMiddleware.run(watchVolunteersAdd);
};
