import { applyMiddleware, compose, createStore, Dispatch, Middleware, Store } from 'redux';
import { composeWithDevTools }                                                from 'redux-devtools-extension';
import { push, routerMiddleware }                                             from 'react-router-redux';
import thunk                                                                  from 'redux-thunk';
import { IAppState, reducers }                                                from './index';
import createHistory                                                          from 'history/createBrowserHistory';
import { IAuthenticationState }                                               from './authenticate/types';
import createSagaMiddleware                                                   from 'redux-saga';
import { sagasRun }                                                           from './sagasRun';

export const browserHistory = createHistory();
const sagaMiddleware = createSagaMiddleware();

const isDevelopment = () =>
{
  const nodeEnv = process.env.NODE_ENV;
  return nodeEnv === 'development';
};

const configureStore = (): Store<IAppState> =>
{
  const composeEnhancers = isDevelopment()
    ? composeWithDevTools({})
    : compose();

  const middlewares: Middleware[] = [
    routerMiddleware(browserHistory),
    sagaMiddleware,
    thunk
  ];

  return createStore<IAppState>(
    reducers,
    composeEnhancers(applyMiddleware(...middlewares))
  );
};

const store = configureStore();

export class State
{
  public static get store(): Store<IAppState>
  {
    return store;
  }

  public static get dispatch(): Dispatch<IAppState>
  {
    return store.dispatch;
  }

  public static routeNavigate(url: string): void
  {
    store.dispatch(push(url));
  }

  public static get authentication(): IAuthenticationState
  {
    return store.getState().authentication;
  }
}

sagasRun(sagaMiddleware);
