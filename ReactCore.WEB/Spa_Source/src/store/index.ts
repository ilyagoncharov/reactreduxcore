import { AnyAction, combineReducers, Reducer } from 'redux';
import { routerReducer, RouterState }          from 'react-router-redux';
import { IAgenciesState }             from './agencies/types';
import { agenciesReducer }            from './agencies/reducer';
import { IVolunteersState }           from './volunteers/types';
import { volunteersReducer }          from './volunteers/reducer';
import { IAuthenticationState }       from './authenticate/types';
import { authenticationReducer }      from './authenticate/reducer';
import { IManagersState }             from './managers/types';
import { managersReducer }            from './managers/reducer';
import { IPageState } from './page/types';
import { pageReducer }                from './page/reducer';

export interface IAppState
{
  routing?: RouterState;
  authentication?: IAuthenticationState;
  page?: IPageState;
  agencies?: IAgenciesState;
  volunteers?: IVolunteersState;
  managers?: IManagersState;
}

export const reducers: Reducer<IAppState> = combineReducers<IAppState>({
  routing: routerReducer,
  authentication: authenticationReducer,
  page: pageReducer,
  agencies: agenciesReducer,
  volunteers: volunteersReducer,
  managers: managersReducer
});

export interface IBaseAction<TActionType> extends AnyAction
{
  type: TActionType;
  payload?: any;
}
