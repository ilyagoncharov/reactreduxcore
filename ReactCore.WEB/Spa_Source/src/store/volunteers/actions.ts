import { VolunteerService }                                                   from '../../services/webapi/volunteer.service';
import { ActionCreator, Dispatch }                                            from 'redux';
import { IActionVolunteersAdd, IActionVolunteersClear, VolunteersActionType } from './types';
import { UserDetails }                                                        from '../../models/dto.models';
import { IAppState }                                                          from '../index';

const service = new VolunteerService();

export const actionCreatorVolunteersAdd: ActionCreator<IActionVolunteersAdd> = (users: UserDetails[]) => ({
  type: VolunteersActionType.VOLUNTEERS_ADD,
  payload: users
});

export const actionCreatorVolunteersClear: ActionCreator<IActionVolunteersClear> = () => ({
  type: VolunteersActionType.VOLUNTEERS_CLEAR
});

export const actionVolunteersAdd = () =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    service.list().subscribe((users: UserDetails) =>
    {
      dispatch(actionCreatorVolunteersAdd(users));
    });
  }
};
