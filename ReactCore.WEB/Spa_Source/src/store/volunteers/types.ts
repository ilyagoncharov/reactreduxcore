import { UserDetails } from '../../models/dto.models';
import { IBaseAction } from '../index';

export interface IVolunteersState
{
  volunteers: UserDetails[];
}

export enum VolunteersSagaType
{
  VOLUNTEERS_SAGA_ADD = 'VOLUNTEERS_SAGA_ADD'
}

export enum VolunteersActionType
{
  VOLUNTEERS_ADD = 'VOLUNTEERS_ADD',
  VOLUNTEERS_CLEAR = 'VOLUNTEERS_CLEAR'
}

export interface IActionVolunteersAdd extends IBaseAction<VolunteersActionType>
{
  type: VolunteersActionType.VOLUNTEERS_ADD,
  payload: UserDetails[];
}

export interface IActionVolunteersClear extends IBaseAction<VolunteersActionType>
{
  type: VolunteersActionType.VOLUNTEERS_CLEAR,
}
