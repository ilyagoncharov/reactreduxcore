import { VolunteersSagaType }         from './types';
import { VolunteerService }           from '../../services/webapi/volunteer.service';
import { call, put, takeEvery }       from 'redux-saga/effects';
import { actionCreatorVolunteersAdd } from './actions';

const service = new VolunteerService();

export const sagaActionVolunteersAdd = () => ({
  type: VolunteersSagaType.VOLUNTEERS_SAGA_ADD
});

function* volunteerAddAsync()
{
  try
  {
    const data = yield call(() => service.list().toPromise());

    yield put(actionCreatorVolunteersAdd(data));
  }
  catch (e)
  {
    console.error('e', e);
  }
}

export function* watchVolunteersAdd()
{
  yield takeEvery(VolunteersSagaType.VOLUNTEERS_SAGA_ADD, volunteerAddAsync);
}
