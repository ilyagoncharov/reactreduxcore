import { ActionCreator, Dispatch }                                                           from 'redux';
import { AuthenticationActionType, IActionAuthenticationLogIn, IActionAuthenticationLogOut } from './types';
import { Authentication }                                                                    from '../../models/auth.models';
import { IAppState }                                                                         from '../index';

export const actionCreatorAuthenticationLogIn: ActionCreator<IActionAuthenticationLogIn> = (auth: Authentication) => ({
  type: AuthenticationActionType.AUTHENTICATION_LOG_IN,
  payload: auth
});

export const actionCreatorAuthenticationLogOut: ActionCreator<IActionAuthenticationLogOut> = () => ({
  type: AuthenticationActionType.AUTHENTICATION_LOG_OUT
});

export const actionAuthenticationLogIn = (auth: Authentication) =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    dispatch(actionCreatorAuthenticationLogIn(auth));
  }
};

export const actionAuthenticationLogOut = () =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    dispatch(actionCreatorAuthenticationLogOut());
  }
};
