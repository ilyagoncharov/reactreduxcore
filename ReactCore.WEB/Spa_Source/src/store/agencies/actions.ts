import { ActionCreator, Dispatch }                                      from 'redux';
import { AgenciesActionType, IActionAgenciesAdd, IActionAgenciesClear } from './types';
import { UserDetails }                                                  from '../../models/dto.models';
import { AgencyService }                                                from '../../services/webapi/agency.service';
import { IAppState }                                                    from '../index';

const service = new AgencyService();

export const actionCreatorAgenciesAdd: ActionCreator<IActionAgenciesAdd> = (users: UserDetails[]) => ({
  type: AgenciesActionType.AGENCIES_ADD,
  payload: users
});

export const actionCreatorAgenciesClear: ActionCreator<IActionAgenciesClear> = () => ({
  type: AgenciesActionType.AGENCIES_CLEAR
});

export const actionAgenciesAdd = () =>
{
  return (dispatch: Dispatch<IAppState>) =>
  {
    service.list().subscribe((users: UserDetails[]) =>
    {
      dispatch(actionCreatorAgenciesAdd(users));
    });
  }
};
